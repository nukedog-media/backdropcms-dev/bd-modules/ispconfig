<?php
/**
 * @file
 * File for handling setting up tables and data to database.
 */

/**
 * Implements hook_schema().
 */
function ispconfig_schema() {
  $schema = array();
  $schema['ispconfig_api_request'] = array(
    'description' => 'A log of the main requests to the ISPConfig API.',
    'fields' => array(
      'rid' => array(
        'description' => 'The primary identifier of request.',
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'uid' => array(
        'description' => 'User Id of API requested.',
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'creation_timestamp' => array(
        'description' => 'API creation timestamp.',
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'function_name' => array(
        'description' => 'Requested API function name.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'serialized_parameters' => array(
        'description' => 'Function parameters',
        'type' => 'text',
        'not null' => TRUE,
        'serialize' => TRUE,
      ),
      'request_status' => array(
        'description' => 'Request status from ISPConfig Server.',
        'type' => 'text',
        'not null' => TRUE,
      ),
      'request_timestamp' => array(
        'description' => 'API request timestamp.',
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'serialized_request_response' => array(
        'description' => 'Last response from ISPConfig server.',
        'type' => 'text',
        'not null' => TRUE,
        'serialize' => TRUE,

      ),
      'request_duration' => array(
        'description' => 'API request response duration.',
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
    ),
    'unique keys' => array(
      'rid' => array('rid'),
    ),
    'primary key' => array('rid'),
  );
  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function ispconfig_uninstall() {
  // Declaring a variable for localization at uninstall time.
  $t = get_t();
  // Remove ISPConfig Settings from table on uninstall.
  variable_del('ispconfig_settings');
  variable_del('ispconfig_functions');
  // Informs users when variable has been removed during uninstall.
  drupal_set_message($t('ISPConfig settings have been removed.'), 'success');
}